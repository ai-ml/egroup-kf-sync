import time
import yaml
from suds.client import Client
from suds.sudsobject import asdict
from prompt_toolkit import prompt
from kubernetes import client as k8s_client
from kubernetes import config

# Before running the script, make sure the nodes are labeled
# The nodes should be labeled: 'k label node NODE_NAME dedicate-tag=EGROUP_TAG'
# k label node NODE_NAME dedicate-tag=ml-3rd-level

def get_egroup_members(egroup):
    """Return the a set of tuples (ID, Type, Name, Email, PrimaryAccount) for a given e-group"""
    try:
        login = 'dgolubov' #prompt(u"Username: ")
        pswd = prompt(u"Password: ", is_password=True)
    except KeyboardInterrupt:
        exit()

    url = "https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl"
    client = Client(url=url, username=login, password=pswd) 
    reply = client.service.FindEgroupByName(egroup)

    result = []

    for member in reply.result.Members:
        if not hasattr(reply, "result"):
            raise RuntimeError("ERROR no result for: %s", egroup)
            
        member = asdict(member)
        if 'PrimaryAccount' not in member:
            raise RuntimeError('Member does not have e-mail column set in e-group: ' + str(member))

        result.append(member.get('PrimaryAccount').lower())
        
    return result
    

def create_profile(k8s_co_client, name, template_file='profile_template.yaml'):
    try:
        with open(template_file, 'r') as f:
            profile_dict = yaml.safe_load(f)
            profile_dict['metadata']['name'] = name
            profile_dict['spec']['owner']['name'] = name

        k8s_co_client.create_cluster_custom_object(
            group='kubeflow.org',
            version='v1',
            plural='profiles',
            body=profile_dict,
        )
    except:
        print("ERROR: Profile creation failed for: " + name)
        return False

    return True


def create_profile_if_not_exists(k8s_co_client, name, template_file='profile_template.yaml'):
    try:
        k8s_co_client.get_cluster_custom_object(
            group='kubeflow.org',
            version='v1',
            plural='profiles',
            name=name,
        )
    except k8s_client.rest.ApiException as e:
        if e.status == 404:
            if create_profile(k8s_co_client, name, template_file):
                print('Profile created for: ' + name)
            else:
                print('ERROR: Profile creation failed for: ' + name)
                raise
        else:
            raise e

    print('Profile exists for: ' + name)


def create_assigns(k8s_co_client, egroup):
    for template_file in ['node_tolerations.yaml', 'node_affinity.yaml']:
        with open(template_file, 'r') as f:
            assign_dict = yaml.safe_load(f)
            assign_dict['metadata']['name'] = assign_dict['metadata']['name'] + '-' + egroup
            assign_dict['spec']['match']['namespaces'] = [egroup]

            if template_file == 'node_tolerations.yaml':
                assign_dict['spec']['parameters']['assign']['value'][0]['key'] = 'dedicate-' + egroup
            else:
                assign_dict['spec']['parameters']['assign']['value'][0]['matchExpressions'][0]['key'] = 'dedicate-' + egroup

            res = k8s_co_client.create_cluster_custom_object(
                group='mutations.gatekeeper.sh',
                version='v1',
                plural='assign',
                body=assign_dict,
            )


def taint_nodes(v1, annotations, egroup):
    node_tags = annotations['node-tags'].split(',')
    taint = k8s_client.V1Taint(key='dedicate-' + egroup, value='true', effect='NoSchedule')

    for node_tag in node_tags:
        nodes = v1.list_node(label_selector='dedicate-tag=' + node_tag)
            
        for node in nodes.items:
            if node.spec.taints and not taint in node.spec.taints:
                node.spec.taints.append(taint)
            else:
                node.spec.taints = [taint]
            if annotations.get('access-only-dedicated', 'false') == 'true':
                node.metadata.labels['dedicate-' + egroup] = 'true'
            v1.patch_node(node.metadata.name, node)


def add_profile_to_assigns(v1, profile, annotations, egroup):
    assign_names = ['node-tolerations-' + egroup]
    if annotations.get('access-only-dedicated', 'false') == 'true':
        assign_names.append('node-affinity-' + egroup)

    for assign_name in assign_names:
        assign = k8s_co_client.get_cluster_custom_object(
            group='mutations.gatekeeper.sh',
            version='v1',
            plural='assign',
            name=assign_name
        )

        assign['spec']['match']['namespaces'].append(profile)

        del assign['metadata']['creationTimestamp']
        del assign['metadata']['resourceVersion']
        del assign['metadata']['selfLink']
        del assign['metadata']['uid']

        print('patching')

        try:
            k8s_co_client.patch_cluster_custom_object(
                group='mutations.gatekeeper.sh',
                version='v1',
                plural='assign',
                name=assign_name,
                body=assign,
            )
        except Exception as e:
            print('ex', e)


#config.load_incluster_config()
config.load_kube_config('/afs/cern.ch/user/d/dgolubov/ml-prod/ml/config')
v1 = k8s_client.CoreV1Api()
k8s_co_client = k8s_client.CustomObjectsApi()

profiles = k8s_co_client.list_cluster_custom_object(group='kubeflow.org', version='v1', plural='profiles')
egroup_profiles = [profile for profile in profiles['items'] if 'annotations' in profile['metadata'] and 'egroup' in profile['metadata']['annotations']]

print('egroup profiles')
for ep in egroup_profiles:
    print(ep['metadata']['name'])

retries = 10

for profile in egroup_profiles:
    annotations = profile['metadata']['annotations']

    try:
        egroup = annotations['egroup']

        if 'node-tags' in annotations:
            print('tainting')
            taint_nodes(v1, annotations, egroup)

        #create_profile_if_not_exists(k8s_co_client, egroup)
        print('creating assigns')
        create_assigns(k8s_co_client, egroup)

        egroup_members = get_egroup_members(egroup)
        print('egroup_members')
        print(egroup_members)
        for member in egroup_members:
            print(member)
            create_profile_if_not_exists(k8s_co_client, member)
            print('patching assigns')
            add_profile_to_assigns(k8s_co_client, member, annotations, egroup)
            pass

        retries = 10
    except Exception as e:
        print("ERROR: Failed to process egroup: " + egroup)
        print(e)
        time.sleep(10)
        retries -= 1
        if retries == 0:
            break


# Read profiles
#   Read annotations from each profile
#   Read egroups from each annotation
#   Read nodes
#       Taint nodes
#   Read egroup members
#       Create profiles for each member if not exist
#       Create Assign for each profile with node affinity
